# Angular password directive

Directive to use in registration form for Password and Retype Password.

# How to install
    npm i ng-password-validation --save

# Path
    <script src="./node_modules/ng-password-validation/ng-password-validation.js" ></script>

# How to use
    js - angular.module("my-app", ['ng-password-validation']);
    html - <password ng-model="password"></password>

# Dependencies
    bootstrap > 3.0.0
    angularjs > 1.x.x

# Required fields
    ng-model
    
# Other properties

| Property | Description / Value | Required |
| -------- | ----- | -------- |
| ng-model | Your-model-name | Yes |
| form-type | form-horizontal / basic / form-inline | No |
| is-required | true / false | No |
| password-label | Label to display for password field | No |
| password-placeholder | Label to set placeholder for password field | No |
| retype-password-label | Label to display for retype password field | No |
| retype-password-placeholder | Label to set placeholder for retype password field | No |
| min-password-length | Number ( for example: 5 ) | No |
| max-password-length | Number ( for example: 15 ) | No |
| allow-uppercase | true / false | No |
| allow-lowercase | true / false | No |
| allow-digit | true / false | No |
| allow-special-character | true / false | No |
| special-characters | Set your own specialcharacter RegEx if you want to override default special characters ( for example: specialCharactersPattern = new RegExp("[@]"); | No |
| view-password | true / false ( Enable view password functionality ) | No |
| is-password-valid | Your model ( In this property, you will get either password is valid or not ) | Yes |
| show-feedback ( coming soon ) | true / false | No |
| show-feedback-with-icon ( coming soon ) | true / false | No |

# Example
    <password 
        ng-model="vm.password" 
        form-type="form-horizontal" 
        
        is-required = "false"
        
        password-label = "Password"
        password-placeholder = "Password"

        retype-password-label = "Retype Password"
        retype-password-placeholder = "Retype Password"

        max-password-length = "12"

        allow-uppercase = "true"
        allow-lowercase = "true"
        allow-digit = "true"
        allow-special-character = "true"

        view-password = "true"
        is-password-valid = "vm.isPasswordValid">
    </password>


# Check above example here
### https://punitsonime.gitlab.io/ng-password-validation

# Note:
    Use this directive in between <form></form> tag

# How to test this directive
 Install browser-sync globally to run local server
 
    - npm i browser-sync -g
    - Now run "npm start" command after installing ng-password-validation package.


