angular
    .module("ng-password-validation", [])
    .directive("password", function () {

        var controller = function ($scope) {

            var vm = $scope.vm;
            vm.form = $scope.form;

            vm.passwordType = "password";
            vm.formType = $scope.formType;
            vm.viewPassword = $scope.viewPassword;
            vm.showPasswordIcon = $scope.viewPassword;

            vm.passwordLabel = $scope.passwordLabel;
            vm.passwordPlaceholder = $scope.passwordPlaceholder;

            vm.retypePasswordLabel = $scope.retypePasswordLabel;
            vm.retypePasswordPlaceholder = $scope.retypePasswordPlaceholder;

            vm.allowUppercase = $scope.allowUppercase;
            vm.allowLowercase = $scope.allowLowercase;
            vm.allowDigit = $scope.allowDigit;
            vm.minPasswordLength = $scope.minPasswordLength;
            vm.maxPasswordLength = $scope.maxPasswordLength;
            vm.allowSpecialCharacter = $scope.allowSpecialCharacter;
            vm.specialCharacters = $scope.specialCharacters;
            vm.isPasswordValid = $scope.isPasswordValid;

            vm.isRequired = $scope.isRequired;
            vm.showFeedback = $scope.showFeedback; //  To highlight border of password and retype password inputs
            vm.showFeedbackWithIcon = $scope.showFeedbackWithIcon; //  To highlight border of password and retype password inputs and display glyphicons

            vm.uppercaseValid = false;
            vm.lowercaseValid = false;
            vm.specialCharacterValid = false;
            vm.digitValid = false;
            vm.minPasswordLengthValid = false;
            vm.maxPasswordLengthValid = false;
            vm.passwordMatchedValid = false;

            vm.showUppercase = ($scope.allowUppercase == true);
            vm.showLowercase = ($scope.allowLowercase == true);
            vm.showSpecialCharacter = ($scope.allowSpecialCharacter == true);
            vm.showDigit = ($scope.allowDigit == true);
            vm.showMinPasswordLength = ($scope.minPasswordLength != null && $scope.minPasswordLength != undefined);
            vm.showMaxPasswordLength = ($scope.maxPasswordLength != null && $scope.maxPasswordLength != undefined);

            vm.customSpecialCharactersSet = false;

            var lowerCaseRegEx = new RegExp("[a-z]");
            var upperCaseRegEx = new RegExp("[A-Z]");
            var digitRegEx = new RegExp("[0-9]");

            var specialCharacterRegEx = new RegExp("[!@#\$%\^&*]");

            if ($scope.allowSpecialCharacter == true && $scope.specialCharacters != null && $scope.specialCharacters != undefined) {
                vm.customSpecialCharactersSet = true;
                specialCharacterRegEx = new RegExp($scope.specialCharacters);
            } else {
                vm.customSpecialCharactersSet = false;
            }

            $scope.password = vm.passwordNgModel;

            //  Functions
            vm.comparePassword = comparePassword;
            vm.changePasswordType = changePasswordType;

            //  Function to compare password and retype password
            function comparePassword(form) {

                //  Maximum length validation
                if (vm.showMaxPasswordLength) {
                    if (vm.passwordNgModel != null && vm.passwordNgModel != undefined && vm.passwordNgModel.trim() != "") {
                        vm.passwordNgModel = vm.passwordNgModel.slice(0, $scope.maxPasswordLength);

                        if (vm.retypePasswordNgModel != null && vm.retypePasswordNgModel != undefined && vm.retypePasswordNgModel.trim() != "") {
                            vm.retypePasswordNgModel = vm.retypePasswordNgModel.slice(0, $scope.maxPasswordLength);
                        }
                        vm.maxPasswordLengthValid = (vm.passwordNgModel.length == $scope.maxPasswordLength);
                    } else {
                        vm.maxPasswordLengthValid = false;
                    }
                } else {
                    vm.maxPasswordLengthValid = true;
                }

                //  Uppercase validation
                if (vm.showUppercase) {
                    if (!!vm.passwordNgModel) {
                        vm.uppercaseValid = upperCaseRegEx.test(vm.passwordNgModel)
                    } else {
                        vm.uppercaseValid = false;
                    }
                }
                else {
                    vm.uppercaseValid = true;
                }

                //  Lowercase validation
                if (vm.showLowercase) {
                    if (!!vm.passwordNgModel) {
                        vm.lowercaseValid = lowerCaseRegEx.test(vm.passwordNgModel)
                    } else {
                        vm.lowercaseValid = false;
                    }
                }
                else {
                    vm.lowercaseValid = true;
                }

                //  Special character validation
                if (vm.showSpecialCharacter) {
                    if (!!vm.passwordNgModel) {
                        vm.specialCharacterValid = specialCharacterRegEx.test(vm.passwordNgModel)
                    } else {
                        vm.specialCharacterValid = false;
                    }
                }
                else {
                    vm.specialCharacterValid = true;
                }
                
                //  Digit validation
                if (vm.showDigit) {
                    if (!!vm.passwordNgModel) {
                        vm.digitValid = digitRegEx.test(vm.passwordNgModel)
                    } else {
                        vm.digitValid = false;
                    }
                }
                else {
                    vm.digitValid = true;
                }
                
                //  Minimum length validation
                vm.minPasswordLengthValid = (vm.showMinPasswordLength) ? vm.passwordNgModel != null && vm.passwordNgModel != undefined && vm.passwordNgModel >= $scope.minPasswordLength : true;

                if (vm.passwordNgModel != null && vm.passwordNgModel != undefined && vm.passwordNgModel.trim() != "" &&
                    vm.retypePasswordNgModel != null && vm.retypePasswordNgModel != undefined && vm.retypePasswordNgModel.trim() != "") {
                    vm.passwordMatchedValid = (vm.passwordNgModel == vm.retypePasswordNgModel);
                } else {
                    vm.passwordMatchedValid = false;
                }

                $scope.ngModel = vm.passwordNgModel;

                $scope.isPasswordValid = vm.isPasswordValid = vm.uppercaseValid == true && vm.lowercaseValid == true && vm.specialCharacterValid == true &&
                    vm.minPasswordLengthValid == true && vm.maxPasswordLengthValid == true && vm.passwordMatchedValid == true;
            }

            //  Function to change input type of password field.
            function changePasswordType() {
                if (vm.passwordType == 'password')
                    vm.passwordType = 'text';
                else
                    vm.passwordType = 'password';
            }
        };

        return {
            transclude: true,
            restrict: "EA",
            replace: false,
            templateUrl: "./ng-password-validation/ng-password-validation.tmpl.html",
            scope: {
                //.."@": One way binding
                //.."=": Two way binding
                form: '=',
                ngModel: "=",

                formType: "@",

                passwordLabel: "@",
                passwordPlaceholder: "@",

                retypePasswordLabel: "@",
                retypePasswordPlaceholder: "@",

                allowUppercase: "=",
                allowLowercase: "=",
                allowDigit: "=",
                minPasswordLength: "=",
                maxPasswordLength: "=",

                allowSpecialCharacter: "=",
                specialCharacters: "=",
                isPasswordValid: "=",

                viewPassword: "=",
                isRequired: "=",

                showFeedback: "=",
                showFeedbackWithIcon: "="
            },
            require: ['^ngModel', '^form'],
            controller: controller,
            controllerAs: "vm",
            link: function (scope, element, attribute, controller) {
                // console.log(scope);
                // console.log(element);
                // console.log(attribute);
                // console.log(controller);
            },
            // compile: function (element, attribute) {
            //     attribute.form = element[0].parentNode.name;
            // }
        }
    });